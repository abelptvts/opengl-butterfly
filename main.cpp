//=============================================================================================
// Szamitogepes grafika hazi feladat keret. Ervenyes 2018-tol.
// A //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// sorokon beluli reszben celszeru garazdalkodni, mert a tobbit ugyis toroljuk.
// A beadott program csak ebben a fajlban lehet, a fajl 1 byte-os ASCII karaktereket tartalmazhat, BOM kihuzando.
// Tilos:
// - mast "beincludolni", illetve mas konyvtarat hasznalni
// - faljmuveleteket vegezni a printf-et kiveve
// - Mashonnan atvett programresszleteket forrasmegjeloles nelkul felhasznalni es
// - felesleges programsorokat a beadott programban hagyni!!!!!!!
// - felesleges kommenteket a beadott programba irni a forrasmegjelolest kommentjeit kiveve
// ---------------------------------------------------------------------------------------------
// A feladatot ANSI C++ nyelvu forditoprogrammal ellenorizzuk, a Visual Studio-hoz kepesti elteresekrol
// es a leggyakoribb hibakrol (pl. ideiglenes objektumot nem lehet referencia tipusnak ertekul adni)
// a hazibeado portal ad egy osszefoglalot.
// ---------------------------------------------------------------------------------------------
// A feladatmegoldasokban csak olyan OpenGL fuggvenyek hasznalhatok, amelyek az oran a feladatkiadasig elhangzottak
// A keretben nem szereplo GLUT fuggvenyek tiltottak.
//
// NYILATKOZAT
// ---------------------------------------------------------------------------------------------
// Nev    : Putovici Ábel
// Neptun : E5V8FJ
// ---------------------------------------------------------------------------------------------
// ezennel kijelentem, hogy a feladatot magam keszitettem, es ha barmilyen segitseget igenybe vettem vagy
// mas szellemi termeket felhasznaltam, akkor a forrast es az atvett reszt kommentekben egyertelmuen jeloltem.
// A forrasmegjeloles kotelme vonatkozik az eloadas foliakat es a targy oktatoi, illetve a
// grafhazi doktor tanacsait kiveve barmilyen csatornan (szoban, irasban, Interneten, stb.) erkezo minden egyeb
// informaciora (keplet, program, algoritmus, stb.). Kijelentem, hogy a forrasmegjelolessel atvett reszeket is ertem,
// azok helyessegere matematikai bizonyitast tudok adni. Tisztaban vagyok azzal, hogy az atvett reszek nem szamitanak
// a sajat kontribucioba, igy a feladat elfogadasarol a tobbi resz mennyisege es minosege alapjan szuletik dontes.
// Tudomasul veszem, hogy a forrasmegjeloles kotelmenek megsertese eseten a hazifeladatra adhato pontokat
// negativ elojellel szamoljak el es ezzel parhuzamosan eljaras is indul velem szemben.
//=============================================================================================
#define _USE_MATH_DEFINES		// Van M_PI
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>

#if defined(__APPLE__)
#include <GLUT/GLUT.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>
#endif
#include <GL/glew.h>		// must be downloaded
#include <GL/freeglut.h>	// must be downloaded unless you have an Apple
#endif

const unsigned int windowWidth = 600, windowHeight = 600;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// You are supposed to modify the code from here...

// OpenGL major and minor versions
int majorVersion = 3, minorVersion = 3;

void getErrorInfo(unsigned int handle) {
    int logLen;
    glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logLen);
    if (logLen > 0) {
        char * log = new char[logLen];
        int written;
        glGetShaderInfoLog(handle, logLen, &written, log);
        printf("Shader log:\n%s", log);
        delete log;
    }
}

// check if shader could be compiled
void checkShader(unsigned int shader, char * message) {
    int OK;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &OK);
    if (!OK) { printf("%s!\n", message); getErrorInfo(shader); }
}

// check if shader could be linked
void checkLinking(unsigned int program) {
    int OK;
    glGetProgramiv(program, GL_LINK_STATUS, &OK);
    if (!OK) { printf("Failed to link shader program!\n"); getErrorInfo(program); }
}

// vertex shader in GLSL
const char * vertexSource = R"(
	#version 330
    precision highp float;

	uniform mat4 MVP;			// Model-View-Projection matrix in row-major format
    uniform vec4 translation;

	layout(location = 0) in vec2 vertexPosition;	// Attrib Array 0
	layout(location = 1) in vec3 vertexColor;	    // Attrib Array 1
	layout(location = 2) in vec2 vertexUV;

	out vec3 color;									// output attribute
    out vec2 texCoord;

	void main() {
		color = vertexColor;														// copy color from input to output
		texCoord = vertexUV;
        gl_Position = vec4(vertexPosition.x, vertexPosition.y, 0, 0) * MVP + translation; 		// transform to clipping space
	}
)";

// fragment shader in GLSL
const char * fragmentSource = R"(
	#version 330
    precision highp float;

	uniform int shouldTexture;

	in vec3 color;				// variable input: interpolated color of vertex shader
	in vec2 texCoord;

    out vec4 fragmentColor;		// output that goes to the raster memory as told by glBindFragDataLocation

    vec4 myFancyProceduralTexture(vec2 loc) {
        float n = pow(loc.x, 2) / 3.14159265 - pow(loc.y, 2) * 1.2;

        if(n < 10 * cos(loc.x)) {
            if(n > -100) {
                return vec4(0.25, 0, 0, 1);
            } else {
                return vec4(0.75, 0.35, 0.16, 1);
            }
        }
        return vec4(0.69, 0.56, 0.11, 1);
    }

	void main() {
        if(shouldTexture == 1) {
            fragmentColor = myFancyProceduralTexture(texCoord);
        } else {
            fragmentColor = vec4(color, 1); // extend RGB to RGBA
        }
	}
)";

// row-major matrix 4x4
struct mat4 {
    float m[4][4];
public:
    mat4() {
        m[0][0] = 1; m[0][1] = 0; m[0][2] = 0; m[0][3] = 0;
        m[1][0] = 0; m[1][1] = 1; m[1][2] = 0; m[1][3] = 0;
        m[2][0] = 0; m[2][1] = 0; m[2][2] = 1; m[2][3] = 0;
        m[3][0] = 0; m[3][1] = 0; m[3][2] = 0; m[3][3] = 1;
    }
    mat4(float m00, float m01, float m02, float m03,
         float m10, float m11, float m12, float m13,
         float m20, float m21, float m22, float m23,
         float m30, float m31, float m32, float m33) {
        m[0][0] = m00; m[0][1] = m01; m[0][2] = m02; m[0][3] = m03;
        m[1][0] = m10; m[1][1] = m11; m[1][2] = m12; m[1][3] = m13;
        m[2][0] = m20; m[2][1] = m21; m[2][2] = m22; m[2][3] = m23;
        m[3][0] = m30; m[3][1] = m31; m[3][2] = m32; m[3][3] = m33;
    }

    mat4 operator*(const mat4& right) const {
        mat4 result;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                result.m[i][j] = 0;
                for (int k = 0; k < 4; k++) result.m[i][j] += m[i][k] * right.m[k][j];
            }
        }
        return result;
    }
    operator float*() { return &m[0][0]; }
};

// 3D point in homogeneous coordinates
struct vec4 {
    float x, y, z, w;
    vec4(float _x = 0, float _y = 0, float _z = 0, float _w = 1) { x = _x; y = _y; z = _z; w = _w; }

    vec4 operator*(const mat4& mat) const {
        return vec4(x * mat.m[0][0] + y * mat.m[1][0] + z * mat.m[2][0] + w * mat.m[3][0],
                    x * mat.m[0][1] + y * mat.m[1][1] + z * mat.m[2][1] + w * mat.m[3][1],
                    x * mat.m[0][2] + y * mat.m[1][2] + z * mat.m[2][2] + w * mat.m[3][2],
                    x * mat.m[0][3] + y * mat.m[1][3] + z * mat.m[2][3] + w * mat.m[3][3]);
    }

    vec4 crossProduct(const vec4& v) {
        return vec4(
                y * v.z - z * v.y,
                z * v.x - x * v.z,
                x * v.y - y * v.x,
                1
        );
    }

    vec4 operator+(const vec4& v) const {
        return vec4(x + v.x, y + v.y, z + v.z, w + v.w);
    }

    vec4 operator-(const vec4& v) const {
        return vec4(x - v.x, y - v.y, z - v.z, w - v.w);
    }
};

vec4 operator*(const float& c, const vec4& v) {
    return vec4(c * v.x, c * v.y);
}

// handle of the shader program
unsigned int shaderProgram;

bool hasStarted = false;
bool isHoldingClick = false;
//float x = 0, y = 0;
vec4 mousePosition = vec4(0, 0);

namespace objects {

    std::vector<float> toArrayXY(std::vector<vec4>& v) {
        std::vector<float> array = std::vector<float>();

        for (auto p : v) {
            array.push_back(p.x);
            array.push_back(p.y);
        }

        return array;
    }

    std::vector<float> toArrayXYZ(std::vector<vec4>& v) {
        std::vector<float> array = std::vector<float>();

        for (auto p : v) {
            array.push_back(p.x);
            array.push_back(p.y);
            array.push_back(p.z);
        }

        return array;
    }

    /**
     * Forrás: előadás dia
     * */
    class BezierCurve {
        std::vector<vec4> cps;	// control pts

        float B(int i, float t) {
            int n = static_cast<int>(cps.size() - 1); // n deg polynomial = n+1 pts!
            float choose = 1;
            for(int j = 1; j <= i; j++) choose *= (float)(n-j+1)/j;
            return static_cast<float>(choose * pow(t, i) * pow(1 - t, n - i));
        }
    public:
        void addControlPoint(vec4 cp) { cps.push_back(cp); }

        vec4 r(float t) {
            vec4 rr(0, 0, 0);
            for(int i = 0; i < cps.size(); i++) rr = rr + B(i,t) * cps[i];
            return rr;
        }
    };

    std::vector<vec4> createBezier(const std::vector<vec4>& points) {
        BezierCurve bezierCurve = BezierCurve();
        for (auto p : points) {
            bezierCurve.addControlPoint(p);
        }

        auto curvePoints = std::vector<vec4>();
        float res = 0.01;
        for (float t = 0; t <= 1; t += res) {
            curvePoints.push_back(bezierCurve.r(t));
        }

        return curvePoints;
    }

    class Object {
    protected:
        unsigned int vao = 0;
        std::vector<vec4> points;
        std::vector<vec4> colors;

        GLenum mode;

        mat4 transform;
        mat4 scaleMatrice;
        mat4 rotationMatriceZ;
        mat4 rotationMatriceY;
        vec4 translation;

    public:
        explicit Object() {}

        explicit Object(std::vector<vec4>& points, GLenum mode) {
            this->points = points;
            this->mode = mode;

            this->translation = vec4(0, 0, 0, 1);
            this->scaleMatrice = mat4(
                    1, 0, 0, 0,
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1
            );
            this->rotationMatriceY = mat4(
                    1, 0, 0, 0,
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1
            );
            this->rotationMatriceZ = mat4(
                    1, 0, 0, 0,
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1
            );
        }

        explicit Object(std::vector<vec4>& points, std::vector<vec4>& colors, GLenum mode) {
            this->points = points;
            this->colors = colors;
            this->mode = mode;

            this->translation = vec4(0, 0, 0, 1);
            this->scaleMatrice = mat4(
                    1, 0, 0, 0,
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1
            );
            this->rotationMatriceY = mat4(
                    1, 0, 0, 0,
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1
            );
            this->rotationMatriceZ = mat4(
                    1, 0, 0, 0,
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1
            );

            //opengl stuff
            glGenVertexArrays(1, &vao);	// create 1 vertex array object
            glBindVertexArray(vao);		// make it active

            unsigned int vbo[2];		// vertex buffer objects
            glGenBuffers(2, &vbo[0]);	// Generate 2 vertex buffer objects

            // vertex coordinates: vbo[0] -> Attrib Array 0 -> vertexPosition of the vertex shader
            glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // make it active, it is an array
            auto p = toArrayXY(points);
            float* vertexCoords = &p[0];	// vertex data on the CPU
            glBufferData(GL_ARRAY_BUFFER,      // copy to tAnimahe GPU
                         2 * points.size() * sizeof(float), // number of the vbo in bytes
                         vertexCoords,		   // address of the data array on the CPU
                         GL_STATIC_DRAW);	   // copy to that part of the memory which is not modified
            // Map Attribute Array 0 to the current bound vertex buffer (vbo[0])
            glEnableVertexAttribArray(0);
            // Data organization of Attribute Array 0
            glVertexAttribPointer(0,			// Attribute Array 0
                                  2, GL_FLOAT,  // components/attribute, component type
                                  GL_FALSE,		// not in fixed point format, do not normalized
                                  0, NULL);     // stride and offset: it is tightly packed

            // vertex colors: vbo[1] -> Attrib Array 1 -> vertexColor of the vertex shader
            glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); // make it active, it is an array
            auto c = toArrayXYZ(colors);
            float* vertexColors = &c[0];
            glBufferData(GL_ARRAY_BUFFER, 3 * colors.size() * sizeof(float), vertexColors, GL_STATIC_DRAW);	// copy to the GPU

            // Map Attribute Array 1 to the current bound vertex buffer (vbo[1])
            glEnableVertexAttribArray(1);  // Vertex position
            // Data organization of Attribute Array 1
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL); // Attribute Array 1, components/attribute, component type, normalize?, tightly packed
        }

    protected:
        virtual void setMVPMatrice() {
            transform = scaleMatrice * rotationMatriceY * rotationMatriceZ;
            int location = glGetUniformLocation(shaderProgram, "MVP");

            if (location >= 0)
                glUniformMatrix4fv(location, 1, GL_TRUE, transform); // set uniform variable MVP to the MVPTransform
            else
                printf("uniform MVP cannot be set\n");
        }

        void setTranslation() {
            int localtion = glGetUniformLocation(shaderProgram, "translation");

            if(localtion >= 0) {
                glUniform4f(localtion, translation.x, translation.y, translation.z, translation.w);
            } else {
                printf("uniform translation cannot be set\n");
            }
        }

        void setShouldTexture(unsigned int should) {
            int location = glGetUniformLocation(shaderProgram, "shouldTexture");

            if(location > 0) {
                glUniform1i(location, should);
            } else {
                printf("uniform shouldTexture cannot be set\n");
            }
        }

    public:
        virtual void animate(float dt) {};

        virtual void scale(float factor) {
            this->scaleMatrice = mat4(
                    factor, 0, 0, 0,
                    0, factor, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1
            );
        }

        virtual void rotateZ(float rad) {
            this->rotationMatriceZ = mat4(
                    cos(rad), sin(rad), 0, 0,
                    -sin(rad), cos(rad), 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1
            );
        }

        virtual void rotateY(float rad) {
            this->rotationMatriceY = mat4(
                    cos(rad), 0, 0, -sin(rad),
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    sin(rad), 0, 0, cos(rad)
            );
        }

        virtual void draw() {
            transform = mat4(
                    1, 0, 0, 0,
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1
            );

            setMVPMatrice();
            setTranslation();
            setShouldTexture(0);
            glBindVertexArray(vao);	// make the vao and its vbos active playing the role of the data source
            glDrawArrays(mode, 0, static_cast<GLsizei>(points.size()));	// draw a single triangle with vertices defined in vao
        }

        virtual void move(float dx, float dy, float dz, float dw) {
            translation = vec4(dx, dy, dz, dw);
        }

        vec4& getPosition() {
            return translation;
        }

    };

    class Wing : public Object {
    private:
        float rad = 0;
        bool direction;
        mat4 wingAnimation = mat4();

        std::vector<vec4> uvs;
    public:
        Wing(
                std::vector<vec4> &points,
                GLenum mode,
                bool direction
        ) : Object(points, mode), direction(direction) {
            for (auto point : points) {
                uvs.push_back(10 * point);
            }

            glGenVertexArrays(1, &vao);
            glBindVertexArray(vao);

            unsigned int vbo[2];
            glGenBuffers(2, vbo);

            glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
            auto p = toArrayXY(points);
            float* vertexCoords = &p[0];
            glBufferData(GL_ARRAY_BUFFER, 2 * points.size() * sizeof(float), vertexCoords, GL_STATIC_DRAW);

            glEnableVertexAttribArray(0);
            glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

            glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
            auto u = toArrayXY(uvs);
            float* uvsPointer = &u[0];
            glBufferData(GL_ARRAY_BUFFER, 2 * uvs.size() * sizeof(float), uvsPointer, GL_STATIC_DRAW);

            glEnableVertexAttribArray(2);
                                // lehet 2
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, NULL);
        }

        void animate(float dt) override {
            dt = !direction ? -dt : dt;
            rad += 5 * dt;

            if(rad > M_PI / 4) {
                direction = false;
            }

            if(rad < - M_PI / 4) {
                direction = true;
            }

            this->moveWing(rad);
        }

        void moveWing(float rad) {
            this->wingAnimation = mat4(
                    cos(rad), 0, 0, -sin(rad),
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    sin(rad), 0, 0, cos(rad)
            );
        }

        void setMVPMatrice() override {
            transform = scaleMatrice * wingAnimation * rotationMatriceY * rotationMatriceZ;
            int location = glGetUniformLocation(shaderProgram, "MVP");

            if (location >= 0)
                glUniformMatrix4fv(location, 1, GL_TRUE, transform); // set uniform variable MVP to the MVPTransform
            else
                printf("uniform MVP cannot be set\n");
        }

        void draw() override {
            transform = mat4(
                    1, 0, 0, 0,
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1
            );

            setMVPMatrice();
            setTranslation();
            setShouldTexture(1);
            glBindVertexArray(vao);	// make the vao and its vbos active playing the role of the data source
            glDrawArrays(mode, 0, static_cast<GLsizei>(points.size()));	// draw a single triangle with vertices defined in vao
        }

    };

    std::vector<vec4> mirror(std::vector<vec4>& points) {
        auto mirroredPoints = points;

        for (auto &mirroredPoint : mirroredPoints) {
            mirroredPoint.x *= -1;
        }

        return mirroredPoints;
    }

    std::vector<vec4> createCircle(const float radius, const unsigned int resolution) {
        auto circle = std::vector<vec4>();

        for (int i = 0; i < resolution; ++i) {
            auto phi = 2 * i * M_PI / resolution;
            circle.push_back(
                    vec4(
                            static_cast<float &&>(radius * cos(phi)),
                            static_cast<float &&>(radius * sin(phi))
                    )
            );
        }

        return circle;
    }

    std::vector<vec4> createEllipse(const float width, const float height, const unsigned int resolution) {
        auto ellipse =std::vector<vec4>();

        for (int i = 0; i <= resolution; ++i) {
            auto phi = 2 * i * M_PI / resolution;
            ellipse.push_back(
                    vec4(
                            static_cast<float &&>(width * cos(phi)),
                            static_cast<float &&>(height * sin(phi))
                    )
            );
        }

        return ellipse;
    }

    class Butterfly : public Object {
    private:
        static constexpr float D = 1.5f;
        static constexpr float drag = 0.5f;

        float phi = 0.0f;
        bool isTurningRight = false;

        vec4 v = vec4(0, 0);
        vec4 a = vec4(0, 0);

        Wing* wing1;
        Wing* wing2;
    public:
        Butterfly(
                std::vector<vec4> &points,
                std::vector<vec4> &colors,
                GLenum mode,
                std::vector<vec4>& wing1Points,
                std::vector<vec4>& wing2Points
        ) : Object(points, colors, mode),
            wing1(new Wing(wing1Points, GL_TRIANGLE_FAN, true)),
            wing2(new Wing(wing2Points, GL_TRIANGLE_FAN, false)) {
        }

        virtual ~Butterfly() {
            delete wing1;
            delete wing2;
        }

        void animate(float dt) override {
            wing1->animate(dt);
            wing2->animate(dt);

            if(hasStarted) {
                if(isHoldingClick) {
                    a = D * (mousePosition - this->getPosition()) - drag * v;
                } else {
                    a = - drag * v;
                }

                v = v + dt * a;

                phi = getDegreeFromVelocities();

                auto aOrt = v.crossProduct(a.crossProduct(v));

                float yDeg = sqrt(pow(aOrt.x, 2.0f) + pow(aOrt.y, 2.0f));
                yDeg = static_cast<float>(yDeg >= M_PI / 1.5 ? M_PI / 1.5 : yDeg);


                float scalarProduct = v.x * a.y - v.y * a.x;

                if(scalarProduct < 0) {
                    isTurningRight = true;
                    this->rotateY(-yDeg);
                } else {
                    isTurningRight = false;
                    this->rotateY(yDeg);
                }

                this->rotateZ(static_cast<float>(phi - M_PI / 2));

                this->move(
                        this->getPosition().x + dt * v.x,
                        this->getPosition().y + dt * v.y,
                        0,
                        1
                );
            }
        }

        void rotateY(float rad) override {
            wing1->rotateY(rad);
            wing2->rotateY(rad);
            Object::rotateY(rad);
        }

        void rotateZ(float rad) override {
            wing1->rotateZ(rad);
            wing2->rotateZ(rad);
            Object::rotateZ(rad);
        }

        void move(float dx, float dy, float dz, float dw) override {
            wing1->move(dx, dy, dz, dw);
            wing2->move(dx, dy, dz, dw);
            Object::move(dx, dy, dz, dw);
        }

        void draw() override {
            if(isTurningRight) {
                wing1->draw();
                wing2->draw();
            } else {
                wing2->draw();
                wing1->draw();
            }

            Object::draw();
        }

        void scale(float factor) {
            wing1->scale(factor);
            wing2->scale(factor);
            Object::scale(factor);
        }

    private:
        float getDegreeFromVelocities() {
            float phi = atan(v.y / v.x);

            if(v.x < 0) {
                phi += M_PI;
            }

            return phi;
        }
    };
    constexpr float Butterfly::D;
    constexpr float Butterfly::drag;
    Butterfly* createButterfly() {
        auto points = objects::createEllipse(0.125, 0.4, 100);
        auto colors = std::vector<vec4>(100, vec4(0.2, 0.2, 0.2));
        auto wingPoints = std::vector<vec4>();

        wingPoints.push_back(vec4(0, 0));
        wingPoints.push_back(vec4(0.5, 2));
        wingPoints.push_back(vec4(2, 1));
        wingPoints.push_back(vec4(2, 0));
        wingPoints.push_back(vec4(3, 0));

        wingPoints.push_back(vec4(-3, 0));

        wingPoints.push_back(vec4(3.5, 0));
        wingPoints.push_back(vec4(0.5, -2));
        wingPoints.push_back(vec4(0, 0));

        auto wingCurve1 = objects::createBezier(wingPoints);
        auto wing1Colors = std::vector<vec4>(wingCurve1.size(), vec4(1, 1, 1));

        auto wingCurve2= objects::mirror(wingCurve1);
        auto wing2Colors = std::vector<vec4>(wingCurve2.size(), vec4(1, 1, 1));

        auto butterfly = new objects::Butterfly(
                points,
                colors,
                GL_TRIANGLE_FAN,
                wingCurve1,
                wingCurve2
        );

        butterfly->scale(0.2);

        return butterfly;
    }

    std::vector<vec4> createPetalPoints(const unsigned int number, const unsigned int resolution) {
        auto petalPoints = std::vector<vec4>();

        for (int i = 0; i < resolution; ++i) {
            auto phi = 2 * i * M_PI / resolution;
            if(number % 2 == 0) {
                petalPoints.push_back(
                        vec4(
                                static_cast<float &&>(abs(sin(number / 2 * phi)) * cos(phi)),
                                static_cast<float &&>(abs(sin(number / 2 * phi)) * sin(phi))
                        )
                );
            } else {
                petalPoints.push_back(
                        vec4(
                                static_cast<float &&>(sin(number * phi) * cos(phi)),
                                static_cast<float &&>(sin(number * phi) * sin(phi))
                        )
                );
            }
        }

        return petalPoints;
    }
    class Flower : public Object {
    private:
        Object* petals;
    public:
        Flower(
                std::vector<vec4> &centerPoints,
                std::vector<vec4> &centerColors,
                std::vector<vec4> &petalPoints,
                std::vector<vec4> &petalColors
        ) : Object(centerPoints, centerColors, GL_TRIANGLE_FAN),
            petals(new Object(petalPoints, petalColors, GL_TRIANGLE_FAN)) {
        }

        virtual ~Flower() {
            delete petals;
        }

        void draw() override {
            petals->draw();
            Object::draw();
        }

        void move(float dx, float dy, float dz, float dw) override {
            petals->move(dx, dy, dz, dw);
            Object::move(dx, dy, dz, dw);
        }

        void scale(float factor) override {
            petals->scale(factor);
            Object::scale(factor);
        }
    };
    Flower* createFlower(const unsigned int numberOfPetals, const float x, const float y, const float scaleFactor) {
        auto centerPoints = createCircle(0.25, 200);
        auto centerColors = std::vector<vec4>(200, vec4(1, 1, 0));
        auto petalPoints = createPetalPoints(numberOfPetals, 200);
        auto petalColors = std::vector<vec4>(200, vec4(1, 1, 1));

        auto flower = new Flower(centerPoints, centerColors, petalPoints, petalColors);
        flower->scale(scaleFactor);
        flower->move(x, y, 0, 1);

        return flower;
    }
}

auto myObjects = std::vector<objects::Object*>();

void onInitialization() {

    myObjects.push_back(objects::createFlower(5, 0.66, 0.66, 0.2));
    myObjects.push_back(objects::createFlower(8, -0.66f, 0.3, 0.16));
    myObjects.push_back(objects::createFlower(13, -0.3f, 0.7, 0.2));

    myObjects.push_back(objects::createButterfly());

    // Create vertex shader from string
    unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    if (!vertexShader) { printf("Error in vertex shader creation\n"); exit(1); }
    glShaderSource(vertexShader, 1, &vertexSource, NULL);
    glCompileShader(vertexShader);
    checkShader(vertexShader, "Vertex shader error");

    // Create fragment shader from string
    unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    if (!fragmentShader) { printf("Error in fragment shader creation\n"); exit(1); }
    glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
    glCompileShader(fragmentShader);
    checkShader(fragmentShader, "Fragment shader error");

    // Attach shaders to a single program
    shaderProgram = glCreateProgram();
    if (!shaderProgram) { printf("Error in shader program creation\n"); exit(1); }
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    // Connect the fragmentColor to the frame buffer memory
    glBindFragDataLocation(shaderProgram, 0, "fragmentColor");	// fragmentColor goes to the frame buffer memory

    // program packaging
    glLinkProgram(shaderProgram);
    checkLinking(shaderProgram);
    // make this program run
    glUseProgram(shaderProgram);
}

void onExit() {
    for (auto object : myObjects) {
        delete object;
    }

    glDeleteProgram(shaderProgram);
    printf("exit");
}

void onDisplay() {
    glClearColor(0.15, 0.69, 0.5, 0);							// background color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the screen
    for (auto object : myObjects) {
        object->draw();
    }
    glutSwapBuffers();									// exchange the two buffers
}

void onKeyboard(unsigned char key, int pX, int pY) {
    if (key == 'd') glutPostRedisplay();         // if d, invalidate display, i.e. redraw
}

void onKeyboardUp(unsigned char key, int pX, int pY) {
}

void onMouse(int button, int state, int pX, int pY) {
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {  // GLUT_LEFT_BUTTON / GLUT_RIGHT_BUTTON and GLUT_DOWN / GLUT_UP
        hasStarted = true;
        isHoldingClick = true;

        float cX = 2.0f * pX / windowWidth - 1;	// flip y axis
        float cY = 1.0f - 2.0f * pY / windowHeight;

        mousePosition.x = cX;
        mousePosition.y = cY;

        glutPostRedisplay();     // redraw
    } else if(state == GLUT_UP) {
        isHoldingClick = false;
    }
}

void onMouseMotion(int pX, int pY) {
    float cX = 2.0f * pX / windowWidth - 1;	// flip y axis
    float cY = 1.0f - 2.0f * pY / windowHeight;

    mousePosition.x = cX;
    mousePosition.y = cY;

    glutPostRedisplay();     // redraw
}

void onIdle() {
    static float tend = 0;
    const float dt = 0.01;
    float tstart = tend;
    tend = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;

    for(float t = tstart; t < tend; t += dt) {
        float Dt = std::min(dt, tend - t);
        for (auto object : myObjects) {
            object->animate(Dt);
        }
    }

    glutPostRedisplay();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Do not touch the code below this line

int main(int argc, char * argv[]) {
    glutInit(&argc, argv);
#if !defined(__APPLE__)
    glutInitContextVersion(majorVersion, minorVersion);
#endif
    glutInitWindowSize(windowWidth, windowHeight);				// Application window is initially of resolution 600x600
    glutInitWindowPosition(100, 100);							// Relative location of the application window
#if defined(__APPLE__)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_3_3_CORE_PROFILE);  // 8 bit R,G,B,A + double buffer + depth buffer
#else
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#endif
    glutCreateWindow(argv[0]);

#if !defined(__APPLE__)
    glewExperimental = true;	// magic
    glewInit();
#endif

    printf("GL Vendor    : %s\n", glGetString(GL_VENDOR));
    printf("GL Renderer  : %s\n", glGetString(GL_RENDERER));
    printf("GL Version (string)  : %s\n", glGetString(GL_VERSION));
    glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
    glGetIntegerv(GL_MINOR_VERSION, &minorVersion);
    printf("GL Version (integer) : %d.%d\n", majorVersion, minorVersion);
    printf("GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    onInitialization();

    glutDisplayFunc(onDisplay);                // Register event handlers
    glutMouseFunc(onMouse);
    glutIdleFunc(onIdle);
    glutKeyboardFunc(onKeyboard);
    glutKeyboardUpFunc(onKeyboardUp);
    glutMotionFunc(onMouseMotion);

    glutMainLoop();
    onExit();
    return 1;
}
