//=============================================================================================
// Szamitogepes grafika hazi feladat keret. Ervenyes 2018-tol.
// A //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// sorokon beluli reszben celszeru garazdalkodni, mert a tobbit ugyis toroljuk.
// A beadott program csak ebben a fajlban lehet, a fajl 1 byte-os ASCII karaktereket tartalmazhat, BOM kihuzando.
// Tilos:
// - mast "beincludolni", illetve mas konyvtarat hasznalni
// - faljmuveleteket vegezni a printf-et kiveve
// - Mashonnan atvett programresszleteket forrasmegjeloles nelkul felhasznalni es
// - felesleges programsorokat a beadott programban hagyni!!!!!!!
// - felesleges kommenteket a beadott programba irni a forrasmegjelolest kommentjeit kiveve
// ---------------------------------------------------------------------------------------------
// A feladatot ANSI C++ nyelvu forditoprogrammal ellenorizzuk, a Visual Studio-hoz kepesti elteresekrol
// es a leggyakoribb hibakrol (pl. ideiglenes objektumot nem lehet referencia tipusnak ertekul adni)
// a hazibeado portal ad egy osszefoglalot.
// ---------------------------------------------------------------------------------------------
// A feladatmegoldasokban csak olyan OpenGL fuggvenyek hasznalhatok, amelyek az oran a feladatkiadasig elhangzottak
// A keretben nem szereplo GLUT fuggvenyek tiltottak.
//
// NYILATKOZAT
// ---------------------------------------------------------------------------------------------
// Nev    :
// Neptun :
// ---------------------------------------------------------------------------------------------
// ezennel kijelentem, hogy a feladatot magam keszitettem, es ha barmilyen segitseget igenybe vettem vagy
// mas szellemi termeket felhasznaltam, akkor a forrast es az atvett reszt kommentekben egyertelmuen jeloltem.
// A forrasmegjeloles kotelme vonatkozik az eloadas foliakat es a targy oktatoi, illetve a
// grafhazi doktor tanacsait kiveve barmilyen csatornan (szoban, irasban, Interneten, stb.) erkezo minden egyeb
// informaciora (keplet, program, algoritmus, stb.). Kijelentem, hogy a forrasmegjelolessel atvett reszeket is ertem,
// azok helyessegere matematikai bizonyitast tudok adni. Tisztaban vagyok azzal, hogy az atvett reszek nem szamitanak
// a sajat kontribucioba, igy a feladat elfogadasarol a tobbi resz mennyisege es minosege alapjan szuletik dontes.
// Tudomasul veszem, hogy a forrasmegjeloles kotelmenek megsertese eseten a hazifeladatra adhato pontokat
// negativ elojellel szamoljak el es ezzel parhuzamosan eljaras is indul velem szemben.
//=============================================================================================
#define _USE_MATH_DEFINES		// Van M_PI
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>

#if defined(__APPLE__)
#include <GLUT/GLUT.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>
#endif
#include <GL/glew.h>		// must be downloaded
#include <GL/freeglut.h>	// must be downloaded unless you have an Apple
#endif

const unsigned int windowWidth = 600, windowHeight = 600;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// You are supposed to modify the code from here...

// OpenGL major and minor versions
int majorVersion = 3, minorVersion = 3;

void getErrorInfo(unsigned int handle) {
    int logLen;
    glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logLen);
    if (logLen > 0) {
        char * log = new char[logLen];
        int written;
        glGetShaderInfoLog(handle, logLen, &written, log);
        printf("Shader log:\n%s", log);
        delete log;
    }
}

// check if shader could be compiled
void checkShader(unsigned int shader, char * message) {
    int OK;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &OK);
    if (!OK) { printf("%s!\n", message); getErrorInfo(shader); }
}

// check if shader could be linked
void checkLinking(unsigned int program) {
    int OK;
    glGetProgramiv(program, GL_LINK_STATUS, &OK);
    if (!OK) { printf("Failed to link shader program!\n"); getErrorInfo(program); }
}

// vertex shader in GLSL
const char * vertexSource = R"(
	#version 330
    precision highp float;

	uniform mat4 MVP;			// Model-View-Projection matrix in row-major format
    uniform vec4 translation;

	layout(location = 0) in vec2 vertexPosition;	// Attrib Array 0
	layout(location = 1) in vec3 vertexColor;	    // Attrib Array 1
	out vec3 color;									// output attribute

	void main() {
		color = vertexColor;														// copy color from input to output
		gl_Position = (vec4(vertexPosition.x, vertexPosition.y, 0, 1) + vec4(0, 0, 0, -1)) * MVP + vec4(0, 0, 0, 1) + translation; 		// transform to clipping space
	}
)";

// fragment shader in GLSL
const char * fragmentSource = R"(
	#version 330
    precision highp float;

	in vec3 color;				// variable input: interpolated color of vertex shader
	out vec4 fragmentColor;		// output that goes to the raster memory as told by glBindFragDataLocation

	void main() {
		fragmentColor = vec4(color, 1); // extend RGB to RGBA
	}
)";

// row-major matrix 4x4
struct mat4 {
    float m[4][4];
public:
    mat4() {}
    mat4(float m00, float m01, float m02, float m03,
         float m10, float m11, float m12, float m13,
         float m20, float m21, float m22, float m23,
         float m30, float m31, float m32, float m33) {
        m[0][0] = m00; m[0][1] = m01; m[0][2] = m02; m[0][3] = m03;
        m[1][0] = m10; m[1][1] = m11; m[1][2] = m12; m[1][3] = m13;
        m[2][0] = m20; m[2][1] = m21; m[2][2] = m22; m[2][3] = m23;
        m[3][0] = m30; m[3][1] = m31; m[3][2] = m32; m[3][3] = m33;
    }

    mat4 operator*(const mat4& right) const {
        mat4 result;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                result.m[i][j] = 0;
                for (int k = 0; k < 4; k++) result.m[i][j] += m[i][k] * right.m[k][j];
            }
        }
        return result;
    }
    operator float*() { return &m[0][0]; }
};

// 3D point in homogeneous coordinates
struct vec4 {
    float x, y, z, w;
    vec4(float _x = 0, float _y = 0, float _z = 0, float _w = 1) { x = _x; y = _y; z = _z; w = _w; }

    vec4 operator*(const mat4& mat) const {
        return vec4(x * mat.m[0][0] + y * mat.m[1][0] + z * mat.m[2][0] + w * mat.m[3][0],
                    x * mat.m[0][1] + y * mat.m[1][1] + z * mat.m[2][1] + w * mat.m[3][1],
                    x * mat.m[0][2] + y * mat.m[1][2] + z * mat.m[2][2] + w * mat.m[3][2],
                    x * mat.m[0][3] + y * mat.m[1][3] + z * mat.m[2][3] + w * mat.m[3][3]);
    }

    vec4 crossProduct(const vec4& v) {
        return vec4(
                y * v.z - z * v.y,
                z * v.x - x * v.z,
                x * v.y - y * v.x,
                1
        );
    }
};

// handle of the shader program
unsigned int shaderProgram;

bool hasStarted = false;
bool isHoldingClick = false;
float x = 0, y = 0;

class Object {
private:
    static constexpr float D = 1.5f;
    static constexpr float drag = 0.5f;

    unsigned int vao = 0;
    float time = 0;
    std::vector<float> points;

    mat4 transform;
    mat4 scaleMatrice;
    mat4 rotationMatriceZ;
    mat4 rotationMatriceY;
    vec4 translation;

    float phi = 0.0f;
    float vx = 0.0f;
    float vy = 0.0f;
    float ax = 0.0f;
    float ay = 0.0f;
public:
    explicit Object(std::vector<float>& points) {
        printf("INIT\n");
        this->points = points;
        this->translation = vec4(0, 0, 0, 0);
        this->scaleMatrice = mat4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
        );
        this->rotationMatriceY = mat4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
        );
        this->rotationMatriceZ = mat4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
        );

        //opengl stuff
        glGenVertexArrays(1, &vao);	// create 1 vertex array object
        glBindVertexArray(vao);		// make it active

        unsigned int vbo[2];		// vertex buffer objects
        glGenBuffers(2, &vbo[0]);	// Generate 2 vertex buffer objects

        // vertex coordinates: vbo[0] -> Attrib Array 0 -> vertexPosition of the vertex shader
        glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // make it active, it is an array
        float* vertexCoords = &points[0];	// vertex data on the CPU
        printf("%f\n", *(vertexCoords+2));
//        printf("%d\n", sizeof(*vertexCoords));
        glBufferData(GL_ARRAY_BUFFER,      // copy to tAnimahe GPU
                     points.size() * sizeof(float), // number of the vbo in bytes
                     vertexCoords,		   // address of the data array on the CPU
                     GL_STATIC_DRAW);	   // copy to that part of the memory which is not modified
        // Map Attribute Array 0 to the current bound vertex buffer (vbo[0])
        glEnableVertexAttribArray(0);
        // Data organization of Attribute Array 0
        glVertexAttribPointer(0,			// Attribute Array 0
                              2, GL_FLOAT,  // components/attribute, component type
                              GL_FALSE,		// not in fixed point format, do not normalized
                              0, NULL);     // stride and offset: it is tightly packed

        // vertex colors: vbo[1] -> Attrib Array 1 -> vertexColor of the vertex shader
        glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); // make it active, it is an array
        float vertexColors[] = { 1, 0, 1,   0, 1, 1,   1, 1, 0,   0, 1, 0,   0, 0, 1,   1, 0, 1};	// vertex data on the CPU
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertexColors), vertexColors, GL_STATIC_DRAW);	// copy to the GPU

        // Map Attribute Array 1 to the current bound vertex buffer (vbo[1])
        glEnableVertexAttribArray(1);  // Vertex position
        // Data organization of Attribute Array 1
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL); // Attribute Array 1, components/attribute, component type, normalize?, tightly packed
    }

private:
    void setMVPMatrice() {
        transform = scaleMatrice * rotationMatriceY * rotationMatriceZ;
        int location = glGetUniformLocation(shaderProgram, "MVP");

        if (location >= 0)
            glUniformMatrix4fv(location, 1, GL_TRUE, transform); // set uniform variable MVP to the MVPTransform
        else
            printf("uniform MVP cannot be set\n");
    }

    void setTranslation() {
        int localtion = glGetUniformLocation(shaderProgram, "translation");

        if(localtion >= 0) {
            glUniform4f(localtion, translation.x, translation.y, translation.z, translation.w);
        } else {
            printf("uniform translation cannot be set\n");
        }
    }

    float getDegreeFromVelocities() {
        float phi = atan(vy / vx);

        if(vx < 0) { // pi/2 - pi
            phi += M_PI;
        }

        return phi;
    }

public:
    void animate(float dt) {
        if(hasStarted) {
            if(isHoldingClick) {
                ax = D * (x - this->getX()) - drag * vx;
                ay = D * (y - this->getY()) - drag * vy;
            } else {
                ax =  - drag * vx;
                ay =  - drag * vy;
            }

            vx += ax * dt;
            vy += ay * dt;

            phi = getDegreeFromVelocities();
//
            vec4 aort = vec4(vx, vy, 0, 1).crossProduct(
                    vec4(ax, ay, 0, 1).crossProduct(vec4(vx, vy, 0, 1))
            );

            float yDeg = sqrt(pow(aort.x, 2.0f) + pow(aort.y, 2.0f));
            yDeg = yDeg >= M_PI / 1.5 ? M_PI / 1.5 : yDeg;

            this->rotateY(yDeg);

            printf("%f\n", sqrt(pow(aort.x, 2.0f) + pow(aort.y, 2.0f)));

            this->rotateZ(static_cast<float>(phi - M_PI / 2));

            this->move(
                    this->getX() + vx * dt,
                    this->getY() + vy * dt,
                    0,
                    0
            );
        }
    }

    void scale(float factor) {
        this->scaleMatrice = mat4(
                factor, 0, 0, 0,
                0, factor, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
        );
    }

    void rotateZ(float rad) {
        this->rotationMatriceZ = mat4(
                cos(rad), sin(rad), 0, 0,
                -sin(rad), cos(rad), 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
        );
    }

    void rotateY(float rad) {
        this->rotationMatriceY = mat4(
                cos(rad), 0, 0, -sin(rad),
                0, 1, 0, 0,
                0, 0, 1, 0,
                sin(rad), 0, 0, cos(rad)
        );
    }

    void draw() {
        transform = mat4(
                0.1, 0, 0, 0,
                0, 0.1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
        );

        setMVPMatrice();
        setTranslation();
        glBindVertexArray(vao);	// make the vao and its vbos active playing the role of the data source
        glDrawArrays(GL_TRIANGLE_FAN, 0, static_cast<GLsizei>(points.size()));	// draw a single triangle with vertices defined in vao
    }

    void move(float dx, float dy, float dz, float dw) {
        translation = vec4(dx, dy, dz, dw);
    }

    float getX() {
        return translation.x;
    }

    float getY() {
        return translation.y;
    }
};

Object* object = nullptr;

void onInitialization() {
    glViewport(0, 0, windowWidth, windowHeight);

    // Create objects by setting up their vertex data on the GPU
//    t2.Create();

    std::vector<float> points = std::vector<float>();
    points.push_back(0);
    points.push_back(0);
    points.push_back(-1);
    points.push_back(-1);
    points.push_back(1);
    points.push_back(-1);
    points.push_back(1);
    points.push_back(1);
    points.push_back(-1);
    points.push_back(1);
    points.push_back(-1);
    points.push_back(-1);

    object = new Object(points);

    object->scale(0.1f);

    // Create vertex shader from string
    unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    if (!vertexShader) { printf("Error in vertex shader creation\n"); exit(1); }
    glShaderSource(vertexShader, 1, &vertexSource, NULL);
    glCompileShader(vertexShader);
    checkShader(vertexShader, "Vertex shader error");

    // Create fragment shader from string
    unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    if (!fragmentShader) { printf("Error in fragment shader creation\n"); exit(1); }
    glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
    glCompileShader(fragmentShader);
    checkShader(fragmentShader, "Fragment shader error");

    // Attach shaders to a single program
    shaderProgram = glCreateProgram();
    if (!shaderProgram) { printf("Error in shader program creation\n"); exit(1); }
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    // Connect the fragmentColor to the frame buffer memory
    glBindFragDataLocation(shaderProgram, 0, "fragmentColor");	// fragmentColor goes to the frame buffer memory

    // program packaging
    glLinkProgram(shaderProgram);
    checkLinking(shaderProgram);
    // make this program run
    glUseProgram(shaderProgram);
}

void onExit() {
    glDeleteProgram(shaderProgram);
    printf("exit");
}

void onDisplay() {
    glClearColor(0, 0, 0, 0);							// background color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the screen
    object->draw();
    glutSwapBuffers();									// exchange the two buffers
}

void onKeyboard(unsigned char key, int pX, int pY) {
    if (key == 'd') glutPostRedisplay();         // if d, invalidate display, i.e. redraw
}

void onKeyboardUp(unsigned char key, int pX, int pY) {
}

void onMouse(int button, int state, int pX, int pY) {
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {  // GLUT_LEFT_BUTTON / GLUT_RIGHT_BUTTON and GLUT_DOWN / GLUT_UP
        hasStarted = true;
        isHoldingClick = true;

        float cX = 2.0f * pX / windowWidth - 1;	// flip y axis
        float cY = 1.0f - 2.0f * pY / windowHeight;

        x = cX;
        y = cY;

        glutPostRedisplay();     // redraw
    } else if(state == GLUT_UP) {
        isHoldingClick = false;
    }
}

void onMouseMotion(int pX, int pY) {
    float cX = 2.0f * pX / windowWidth - 1;	// flip y axis
    float cY = 1.0f - 2.0f * pY / windowHeight;

    x = cX;
    y = cY;

    glutPostRedisplay();     // redraw
}

void onIdle() {
    static float tend = 0;
    const float dt = 0.01;
    float tstart = tend;
    tend = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;

    for(float t = tstart; t < tend; t += dt) {
        float Dt = std::min(dt, tend - t);
        object->animate(Dt);
    }

    glutPostRedisplay();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Do not touch the code below this line

int main(int argc, char * argv[]) {
    glutInit(&argc, argv);
#if !defined(__APPLE__)
    glutInitContextVersion(majorVersion, minorVersion);
#endif
    glutInitWindowSize(windowWidth, windowHeight);				// Application window is initially of resolution 600x600
    glutInitWindowPosition(100, 100);							// Relative location of the application window
#if defined(__APPLE__)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_3_3_CORE_PROFILE);  // 8 bit R,G,B,A + double buffer + depth buffer
#else
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#endif
    glutCreateWindow(argv[0]);

#if !defined(__APPLE__)
    glewExperimental = true;	// magic
    glewInit();
#endif

    printf("GL Vendor    : %s\n", glGetString(GL_VENDOR));
    printf("GL Renderer  : %s\n", glGetString(GL_RENDERER));
    printf("GL Version (string)  : %s\n", glGetString(GL_VERSION));
    glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
    glGetIntegerv(GL_MINOR_VERSION, &minorVersion);
    printf("GL Version (integer) : %d.%d\n", majorVersion, minorVersion);
    printf("GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    onInitialization();

    glutDisplayFunc(onDisplay);                // Register event handlers
    glutMouseFunc(onMouse);
    glutIdleFunc(onIdle);
    glutKeyboardFunc(onKeyboard);
    glutKeyboardUpFunc(onKeyboardUp);
    glutMotionFunc(onMouseMotion);

    glutMainLoop();
    onExit();
    return 1;
}
